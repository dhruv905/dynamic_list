package com.example.dhruvdixit.dynamic_list;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
ArrayList item=new ArrayList();
    EditText editText;
    Button button;
    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    editText= (EditText) findViewById(R.id.edittext);
    button= (Button) findViewById(R.id.button);
    listView= (ListView) findViewById(R.id.listview);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              item.add( editText.getText());
                ArrayAdapter adapter=new ArrayAdapter(MainActivity.this,android.R.layout.simple_list_item_checked,item);
                listView.setAdapter(adapter);
          editText.setText("");
                if(item.size()>=5)
                {
                   item.removeAll(item);
                }
            }
        });

    }
}
